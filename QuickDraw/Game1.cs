﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using System;

namespace QuickDraw
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //Load game assets
        Texture2D buttontexture;
        SpriteFont gamefont;
        SoundEffect clickSFX;
        SoundEffect SignalSFX;
        Song gameMusic;


        //gamestate/ input
        MouseState PreviousState;
        bool start = false;
        bool wait = false;
        float CountDown = 0f;
        float StopWatch = 0f;
        bool Countup = false;
        Random rand = new Random();

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            //load button Graphic
            buttontexture = Content.Load<Texture2D>("graphics/button");

            //load gamefont
            gamefont = Content.Load<SpriteFont>("fonts/mainFont");

            //load sfx
            clickSFX = Content.Load<SoundEffect>("audio/buttonclick");
            SignalSFX = Content.Load<SoundEffect>("audio/Signal");

            //load Music
            gameMusic = Content.Load<Song>("audio/Music");

            //Start background music
            MediaPlayer.Play(gameMusic);
            MediaPlayer.IsRepeating = true;

            //load mosue
            IsMouseVisible = true;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            //get current Mouse State
            MouseState currentState = Mouse.GetState();
            MouseState Previoustate; 

            //find center of screen
            Vector2 screenCenter = new Vector2(Window.ClientBounds.Width / 2, Window.ClientBounds.Height / 2);

           

            //determine button rectangle
            Rectangle buttonRectangle = new Rectangle(
                (int)screenCenter.X - buttontexture.Width,
                (int)screenCenter.Y - buttontexture.Height,
                buttontexture.Width,
                buttontexture.Height);

      

            if (currentState.LeftButton == ButtonState.Pressed
               && PreviousState.LeftButton != ButtonState.Pressed
               && buttonRectangle.Contains(currentState.X, currentState.Y))
            {
                //mouse is pressed
                clickSFX.Play();
                if (start == true && Countup == true)
                {
                    Countup = false;
                    start = false;
                }

                if (Countup == false & wait == true)
                {
                    start = false;
                }

                //set start to true
                start = true;
                wait = true;
                CountDown = rand.Next(3, 11);
                StopWatch = 0f;
                

            }
            PreviousState = currentState;

            if (wait == true)
            {
                CountDown -= (float)gameTime.ElapsedGameTime.TotalSeconds;

                if (CountDown <= 0)
                {
                    wait = false;
                    Countup = true;
                    SignalSFX.Play();
                }
            }

            

            if (Countup == true && start == true)
            {
                StopWatch += (float)gameTime.ElapsedGameTime.TotalSeconds;
            }

                base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();

            Vector2 screenCenter = new Vector2(Window.ClientBounds.Width / 2, Window.ClientBounds.Height / 2);

            string promtString = "click the button to start";
            Vector2 promptSize = gamefont.MeasureString(promtString);

            Vector2 TitleSize = gamefont.MeasureString("Quickdraw");

            

           

            spriteBatch.DrawString(
                gamefont,
                "Quickdraw",
                screenCenter - new Vector2(0, 100) - TitleSize / 2,
                Color.White);

            Vector2 AuthorName = gamefont.MeasureString("by logan Weeden");

            spriteBatch.DrawString(
                gamefont,
                "By Logan Weeden",
                screenCenter - new Vector2(0, 75) - TitleSize / 2,
                Color.White);



        
            
                //draw button
                spriteBatch.Draw(buttontexture,
               new Rectangle(
               (int)screenCenter.X - buttontexture.Width,
               (int)screenCenter.Y - buttontexture.Height,
               buttontexture.Width,
               buttontexture.Height),
               Color.White
               );
            

            if (wait == true)
            { 
                promtString = "wait";

            }

            if (Countup == true)
            {
                promtString = "click";
            }

            spriteBatch.DrawString(gamefont, "Timer:", new Vector2(Window.ClientBounds.Width - 150, 10), Color.White);
            spriteBatch.DrawString(gamefont, StopWatch.ToString(), new Vector2(Window.ClientBounds.Width - 50, 10), Color.White);

            if (start == false && Countup == false && wait == true)
            {
                promtString = "you clicked to early";
                    }
            spriteBatch.DrawString(
                gamefont,
               promtString,
                screenCenter - new Vector2(0, 60) - TitleSize / 2,
                Color.Black);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
